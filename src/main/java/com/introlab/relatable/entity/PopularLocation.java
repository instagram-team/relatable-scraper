package com.introlab.relatable.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
//@AllArgsConstructor
@NoArgsConstructor
public class PopularLocation extends PopularObject{
    @Id
//    @GeneratedValue
//    private Integer id;

    private String name;

    public PopularLocation(String name) {
        this.name = name;
    }
}
