package com.introlab.relatable.util;

import com.introlab.relatable.entity.CommonData;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Set;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ScrapeUtlis {
    public static CommonData getCommonData(Document document) {
        Map<String, String> categories = getCategories(document);
        Map<String, String> locations = getLocations(document);

        CommonData commonData = new CommonData();
        Elements generalInfo = document.select(".info-column ul:nth-of-type(1) li");
        for (Element element : generalInfo) {
            if (element.text().contains("influencers")) {
                commonData.setInfluencersCount(Integer.valueOf(element.text().replaceAll("\\D+", "")));
            }
            if (element.text().contains("engagement")) {
                commonData.setAverageEngagement(Double.valueOf(element.text().replaceAll("\\D+", "")) * 0.01);
            }
            if (element.text().contains("followers")) {
                commonData.setAverageFollowersCount(Integer.valueOf(element.text().replaceAll("\\D+", "")));
            }
        }

        Pattern pattern = Pattern.compile("between\\s([\\w+,]+)\\sand\\s(\\w+)\\sfollowers");
        Matcher matcher = pattern.matcher(document.select(".main > p:first-of-type").text());

        if (matcher.find()) {
            String min = matcher.group(1).replace("k", "000").replace("m", "000000");
            String max = matcher.group(2).replace("k", "000").replace("m", "000000");
            commonData.setMinFollowers(Integer.valueOf(min.replaceAll("\\D+", "")));
            commonData.setMaxFollowers(Integer.valueOf(max.replaceAll("\\D+", "")));
            System.out.println();
        }

        commonData.setPopularCategories(getPopularValuesFromMap(categories, document));
        commonData.setPopularLocations(getPopularValuesFromMap(locations, document));
        commonData.setPopularTags(getHashTags(document));
        return commonData;
    }


    private static Set<String> getPopularValuesFromMap(Map<String, String> commonMap, Document document) {

        return document.select(".main a")
                .stream()
                .filter(element -> commonMap.containsKey(element.attr("abs:href")))
                .map(Element::text)
                .collect(Collectors.toSet());
    }

    public static Map<String, String> getCategories(Document document) {
        return document.select(".info-column h3:contains(category) + ul li a").stream().collect(Collectors.toMap(a -> a.attr("abs:href"), Element::text));
    }

    public static Map<String, String> getLocations(Document document) {
        return document.select(".info-column h3:contains(location) + ul li a").stream().collect(Collectors.toMap(a -> a.attr("abs:href"), Element::text));
    }

    public static Set<String> getHashTags(Document document) {
        return document.select(".hashtags-a").stream().map(Element::text).collect(Collectors.toSet());
    }
}
