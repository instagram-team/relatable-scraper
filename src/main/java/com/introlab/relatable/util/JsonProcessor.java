package com.introlab.relatable.util;

import com.introlab.relatable.entity.*;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.util.List;
import java.util.Set;

public class JsonProcessor {

    public static JsonArrayBuilder categoriesToJsonArray(Set<Category> categories) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (Category category : categories) {
            JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
            objectBuilder.add(category.getName(),
                    Json.createObjectBuilder().add("data", commonDataToJsonObject(category.getData())));
            arrayBuilder.add(objectBuilder);
        }
        return arrayBuilder;
    }

    public static JsonArrayBuilder citiesToJsonArray(Set<City> cities) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (City city : cities) {
            JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
            objectBuilder.add(city.getName(),
                    Json.createObjectBuilder().add("data", commonDataToJsonObject(city.getData())));
            objectBuilder.add("categories", categoriesToJsonArray(city.getCategories()));
            arrayBuilder.add(objectBuilder);
        }
        return arrayBuilder;
    }

    public static JsonObjectBuilder commonDataToJsonObject(CommonData data) {
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();

        objectBuilder.add("min_followers", data.getMinFollowers());
        objectBuilder.add("max_followers", data.getMaxFollowers());
        objectBuilder.add("influencers_count", data.getInfluencersCount());
        objectBuilder.add("average_followers_count", data.getAverageFollowersCount());
        objectBuilder.add("average_engagement", data.getAverageEngagement());
        objectBuilder.add("popular_locations", popularObjectsToJsonArray(data.getPopularLocations()));
        objectBuilder.add("popular_categories", popularObjectsToJsonArray(data.getPopularCategories()));
        objectBuilder.add("popular_hashtags", popularObjectsToJsonArray(data.getPopularTags()));

        return objectBuilder;
    }

    public static JsonObject countriesToJsonObject(List<Country> countries) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (Country country : countries) {
            arrayBuilder.add(countryToJsonObject(country));
        }
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
        objectBuilder.add("countries", arrayBuilder);
        return objectBuilder.build();
    }

    public static JsonObjectBuilder countryToJsonObject(Country country) {
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
        objectBuilder.add(country.getName(),
                Json.createObjectBuilder().add("data", commonDataToJsonObject(country.getData())));
        objectBuilder.add("categories", categoriesToJsonArray(country.getCategories()));
        objectBuilder.add("cities", citiesToJsonArray(country.getCities()));
        objectBuilder.add("states", statesToJsonArray(country.getStates()));

        return objectBuilder;
    }

    public static JsonArrayBuilder popularObjectsToJsonArray(Set<?> objects) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        objects.forEach(object -> {
            if (object instanceof PopularObject) {
                arrayBuilder.add(((PopularObject) object).getName());
            }
        });
        return arrayBuilder;
    }

    public static JsonArrayBuilder statesToJsonArray(Set<State> cities) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (State city : cities) {
            JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
            objectBuilder.add(city.getName(),
                    Json.createObjectBuilder().add("data", commonDataToJsonObject(city.getData())));
            objectBuilder.add("categories", categoriesToJsonArray(city.getCategories()));
            objectBuilder.add("cities", citiesToJsonArray(city.getCities()));
            arrayBuilder.add(objectBuilder);
        }
        return arrayBuilder;
    }
}
