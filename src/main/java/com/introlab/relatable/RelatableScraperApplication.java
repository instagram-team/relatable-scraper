package com.introlab.relatable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RelatableScraperApplication {

    public static void main(String[] args) {
        SpringApplication.run(RelatableScraperApplication.class, args);
    }
}
