package com.introlab.relatable.repository;


import com.introlab.relatable.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country,String> {
}
