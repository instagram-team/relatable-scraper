package com.introlab.relatable.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class JsoupService {

    public Document getDocument(String url) throws IOException {
        System.out.println(url);
        return Jsoup.connect(url).get();
    }
}
