package com.introlab.relatable.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PopularObject {
    private String name;
}
