package com.introlab.relatable.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class State {

    @Id
    private String link;
    private String name;
    @OneToOne
    private CommonData data;
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.EAGER
    )
    private Set<City> cities;
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.EAGER
    )
    private Set<Category> categories;
}
