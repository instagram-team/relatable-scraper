package com.introlab.relatable.repository;


import com.introlab.relatable.entity.Category;
import com.introlab.relatable.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category,String> {
}
