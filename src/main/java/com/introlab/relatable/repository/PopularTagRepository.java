package com.introlab.relatable.repository;


import com.introlab.relatable.entity.PopularTag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PopularTagRepository extends JpaRepository<PopularTag, String> {
}
