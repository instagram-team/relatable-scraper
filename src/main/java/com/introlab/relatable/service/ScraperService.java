package com.introlab.relatable.service;

import com.introlab.relatable.entity.*;
import com.introlab.relatable.repository.*;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static com.introlab.relatable.util.ScrapeUtlis.*;

@Service
public class ScraperService {

    private final CountryRepository countryRepository;

    private final JsoupService jsoupService;

    @Autowired
    private PopularCategoryRepository popularCategoryRepository;

    @Autowired
    private PopularLocationRepository popularLocationRepository;

    @Autowired
    private PopularTagRepository popularTagRepository;

    @Autowired
    private CommonDataRepository commonDataRepository;

    @Autowired
    public ScraperService(JsoupService jsoupService, CountryRepository countryRepository) {
        this.jsoupService = jsoupService;
        this.countryRepository = countryRepository;
    }

    public void scrapeCountry(String url) throws IOException {
        System.out.println(url);
        if (url.contains("united-states")||countryRepository.findById(url).isPresent()){
            return;
        }
        Document document = jsoupService.getDocument(url);
        String countryName = document.select(".breadcrumbs a:last-of-type").text();
        CommonData commonData = getCommonData(document);
        commonData.setLink(url);
        Country country = new Country();
        country.setName(countryName);
        country.setLink(url);
        country.setData(commonData);
        savePopularData(country.getData());
        Set<Category> categories = getCategories(document).keySet().parallelStream().map(this::scrapeCategory).collect(Collectors.toSet());
        country.setCategories(categories);

        if (!url.contains("united-states")) {
            Set<City> cities = getLocations(document).keySet().parallelStream().map(this::scrapeCity).collect(Collectors.toSet());
            country.setCities(cities);
            country.setStates(new TreeSet<>());
        } else {
            Set<State> cities = getLocations(document).keySet().parallelStream().map(this::scrapeState).collect(Collectors.toSet());
            country.setStates(cities);
            country.setCities(new TreeSet<>());
        }
        countryRepository.save(country);

        System.out.println();
    }

    public City scrapeCity(String url) {
        Document document = null;
        try {
            document = jsoupService.getDocument(url);

            String cityName = document.select(".breadcrumbs a:last-of-type").text();

            City city = new City();
            city.setLink(url);
            city.setName(cityName);
            CommonData commonData = getCommonData(document);
            commonData.setLink(url);
            city.setData(commonData);
            savePopularData(city.getData());
            Set<Category> categories = getCategories(document).keySet().parallelStream().map(this::scrapeCategory).collect(Collectors.toSet());
            city.setCategories(categories);
            return city;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public State scrapeState(String url) {
        Document document = null;
        try {
            document = jsoupService.getDocument(url);

            String cityName = document.select(".breadcrumbs a:last-of-type").text();

            State state = new State();
            state.setLink(url);
            state.setName(cityName);
            CommonData commonData = getCommonData(document);
            commonData.setLink(url);
            state.setData(commonData);
            savePopularData(state.getData());

            Set<City> cities = getLocations(document).keySet().parallelStream().map(this::scrapeCity).collect(Collectors.toSet());
            state.setCities(cities);

            Set<Category> categories = getCategories(document).keySet().parallelStream().map(this::scrapeCategory).collect(Collectors.toSet());
            state.setCategories(categories);
            return state;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Category scrapeCategory(String url) {
        Document document;
        try {
            document = jsoupService.getDocument(url);

            String categoryName = document.select(".breadcrumbs a:last-of-type").text();
            Category category = new Category();
            category.setName(categoryName);
            category.setLink(url);
            CommonData commonData = getCommonData(document);
            commonData.setLink(url);
            category.setData(commonData);
            savePopularData(category.getData());
            return category;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    private void savePopularData(CommonData commonData) {
        savePopularObjects(commonData.getPopularCategories());
        savePopularObjects(commonData.getPopularLocations());
        savePopularObjects(commonData.getPopularTags());
        if (!commonDataRepository.findById(commonData.getLink()).isPresent()) {
            commonDataRepository.save(commonData);
        }
    }

    private void savePopularObjects(Set<?> objects) {

        for (Object object : objects) {
            if (object instanceof PopularCategory) {
                if (!popularCategoryRepository.findById(((PopularCategory) object).getName()).isPresent()) {
                    popularCategoryRepository.save((PopularCategory) object);
                }
            }
            if (object instanceof PopularLocation) {
                if (!popularLocationRepository.findById(((PopularLocation) object).getName()).isPresent()) {
                    popularLocationRepository.save((PopularLocation) object);
                }
            }
            if (object instanceof PopularTag) {
                if (!popularTagRepository.findById(((PopularTag) object).getName()).isPresent()) {
                    popularTagRepository.save((PopularTag) object);
                }
            }
        }

    }


}
