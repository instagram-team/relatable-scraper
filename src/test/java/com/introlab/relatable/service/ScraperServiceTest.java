package com.introlab.relatable.service;

import com.introlab.relatable.RelatableScraperApplicationTests;
import com.introlab.relatable.entity.Country;
import com.introlab.relatable.repository.CategoryRepository;
import com.introlab.relatable.repository.CountryRepository;
import org.jsoup.nodes.Element;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.json.JsonObject;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import static com.introlab.relatable.util.JsonProcessor.countriesToJsonObject;

public class ScraperServiceTest extends RelatableScraperApplicationTests {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private JsoupService jsoupService;
    @Autowired
    private ScraperService scraperService;

    @Test
    public void test() throws IOException {

//        scraperService.scrapeCountry("https://influencers.relatable.me/influencers-on-instagram-in-andorra");
//        System.out.println("one done");
//        scraperService.scrapeCountry("https://influencers.relatable.me/influencers-on-instagram-in-ukraine");
//        System.out.println("two done");
//        scraperService.scrapeCountry("https://influencers.relatable.me/influencers-on-instagram-in-sweden");
//        System.out.println("three done");
//        scraperService.scrapeCountry("https://influencers.relatable.me/influencers-on-instagram-in-united-states");
        for (Element element : jsoupService.getDocument("https://influencers.relatable.me").select(".main a")) {
            scraperService.scrapeCountry(element.attr("abs:href"));
        }

    }


    @Test
    public void testDBFetch() throws IOException {

        List<Country> all = countryRepository.findAll();
//        for (Country country : all) {
//            JsonObject jsonObject = countryToJsonArray(country);
//            try (FileWriter file = new FileWriter(country.getName() + ".json")) {
//                file.write(jsonObject.toString());
//                System.out.println("Successfully Copied JSON Object to File...");
//            }
//        }

        JsonObject jsonValues = countriesToJsonObject(all);
        try (FileWriter file = new FileWriter("relatable-full.json")) {
            file.write(jsonValues.toString());
            System.out.println("Successfully Copied JSON Object to File...");
        }
        System.out.println(all);
    }
}