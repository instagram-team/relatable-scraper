package com.introlab.relatable.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class PopularTag extends PopularObject{
    @Id
//    @GeneratedValue
//    private Integer id;

    private String name;

    public PopularTag(String name) {
        this.name = name;
    }
}
