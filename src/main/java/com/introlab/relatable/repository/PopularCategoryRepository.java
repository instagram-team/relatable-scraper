package com.introlab.relatable.repository;


import com.introlab.relatable.entity.PopularCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PopularCategoryRepository extends JpaRepository<PopularCategory, String> {

}
