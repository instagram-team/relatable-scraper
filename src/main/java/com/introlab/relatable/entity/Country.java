package com.introlab.relatable.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Country {

    @Id
    private String link;

    private String name;

    @OneToOne(
            cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.EAGER
    )
    private CommonData data;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.EAGER
    )
    private Set<City> cities;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.EAGER
    )
    private Set<Category> categories;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.EAGER
    )
    private Set<State> states;
}
