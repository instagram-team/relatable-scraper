package com.introlab.relatable.repository;


import com.introlab.relatable.entity.CommonData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonDataRepository extends JpaRepository<CommonData, String> {
}
