package com.introlab.relatable.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
public class CommonData {
    @Id
    private String link;
    private Integer minFollowers;
    private Integer maxFollowers;
    private Integer influencersCount;
    private Integer averageFollowersCount;
    private Double averageEngagement;
    @ManyToMany(
            cascade = {CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.REMOVE}
            , fetch = FetchType.EAGER)
    private Set<PopularCategory> popularCategories;
    @ManyToMany(
            cascade = {CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.REMOVE}
            , fetch = FetchType.EAGER)
    private Set<PopularLocation> popularLocations;
    @ManyToMany(
            cascade = {CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.REMOVE}
            , fetch = FetchType.EAGER)
    private Set<PopularTag> popularTags;

    public void setPopularCategories(Set<String> popularCategories) {
        this.popularCategories = popularCategories.stream().map(PopularCategory::new).collect(Collectors.toSet());
    }

    public void setPopularLocations(Set<String> popularLocations) {
        this.popularLocations = popularLocations.stream().map(PopularLocation::new).collect(Collectors.toSet());
    }

    public void setPopularTags(Set<String> popularTags) {
        this.popularTags = popularTags.stream().map(PopularTag::new).collect(Collectors.toSet());
    }
}
