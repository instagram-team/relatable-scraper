package com.introlab.relatable.repository;


import com.introlab.relatable.entity.PopularLocation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PopularLocationRepository extends JpaRepository<PopularLocation, String> {
}
